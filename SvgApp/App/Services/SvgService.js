﻿(function () {
    'use strict';
    angular.module('svgApp')
        .service('svgService', [
            '$http', 'svgApi',
            function ($http, svgApi) {
                this.action = 'images/';
                this.add = function (image) {
                    var request = $http({
                        method: 'POST',
                        url: svgApi.apiUrl + this.action,
                        data: JSON.stringify(image),
                        dataType: "json"
                    });
                    return request;
                }

                this.update = function (image) {
                    var request = $http({
                        method: 'PUT',
                        url: svgApi.apiUrl + this.action + image.Id,
                        data: JSON.stringify(image),
                        dataType: "json"
                    });
                    return request;
                }

                this.delete = function (id) {
                    var request = $http({
                        method: 'DELETE',
                        url: svgApi.apiUrl + this.action + id,
                        dataType: "json"
                    });
                    return request;
                }

                this.getAll = function () {
                    var request = $http({
                        method: 'GET',
                        url: svgApi.apiUrl + this.action,
                        dataType: "json"
                    });
                    return request;
                }
                
                this.get = function (id) {
                    var request = $http({
                        method: 'GET',
                        url: svgApi.apiUrl + this.action + id,
                        dataType: "json"
                    });
                    return request;
                }

                this.generatePdf = function () {
                    var request = $http({
                        method: 'GET',
                        url: svgApi.apiUrl + this.action + 'pdf',
                        dataType: "application/pdf",
                        responseType: 'arraybuffer'
                    });
                    return request;
                }
            }
        ]);
})()

    