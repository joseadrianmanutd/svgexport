﻿(function () {
    'use strict';
    angular.module('svgApp')
        .controller('ImagesController',
        ['svgService', '$mdDialog', '$scope',
            function (svgService, $mdDialog, $scope) {
            var $ctrl = this;
            getImages();
                
            $ctrl.delete = function (id) {
                var confirm = $mdDialog.confirm()
                    .title('Would you like to delete the record?')
                    .textContent('')
                    .ariaLabel('')
                    .ok('Yes')
                    .cancel('No');

                $mdDialog.show(confirm).then(function () {
                    svgService.delete(id)
                        .then(function (response) {
                            getImages();
                }, function () {
                });                
                }, function () {
                });
            };

            function getImages() {
                svgService.getAll().then(function (response) {
                    $ctrl.images = response.data;
                }, function () {

                })
            };

            $ctrl.generatePdf = function() {
                svgService.generatePdf().then(function (response) {
                    var blob = new Blob([new Uint8Array(response.data)], { type: 'application/pdf' });
                    $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                    window.open($scope.url);	
                }, function (response) {
                    //Show error
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent('Invalid SVG definitions.')
                            .ok('DISMISS')
                    );
                })
            };

            $ctrl.showModal = function showImagesModal(id) {
                $mdDialog.show({
                    templateUrl: '/App/Views/Modals/imagesEditorModal.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: true,
                    controllerAs: '$editorModal',
                    controller: function () {
                        var $editorCtrlModal = this;

                        if (id !== undefined) {
                            svgService.get(id).then(function (response) {
                                $editorCtrlModal.image = response.data;
                            }, function () {
                            });
                        }                        
                       
                        $editorCtrlModal.save = function () {
                            var model = {
                                Id: $editorCtrlModal.image.Id,
                                Title: $editorCtrlModal.image.Title,
                                SvgString: $editorCtrlModal.image.SvgString
                            };
                            if (model.Id === undefined) {
                                svgService.add(model)
                                    .then(function (response) {  
                                        $mdDialog.hide(true);
                                    }, function (response) {
                                        //Show error
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .parent(angular.element(document.querySelector('#popupContainer')))
                                                .clickOutsideToClose(true)
                                                .title('Error')
                                                .textContent(response.data.Message)
                                                .ok('DISMISS')
                                        );
                                    });
                            }
                            else {
                                svgService.update(model)
                                    .then(function (response) {                                       
                                        $mdDialog.hide(true);
                                    }, function (response) {
                                        //Show error
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .parent(angular.element(document.querySelector('#popupContainer')))
                                                .clickOutsideToClose(true)
                                                .title('Error')
                                                .textContent(response.data.Message)
                                                .ok('DISMISS')
                                        );
                                    });
                            }
                        };

                        $editorCtrlModal.close = function () {
                            $mdDialog.hide(true);
                        };
                    }
                }).then(function (success) {
                    getImages();
                }).then(function (cancelData) {

                });
            }
        }
        ]);
})();