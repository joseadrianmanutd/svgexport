﻿(function () {
    'use strict';
    angular.module("svgApp").config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "App/Views/images.html",
                controller: 'ImagesController',
                controllerAs: '$ctrl',
            });
    });
})();