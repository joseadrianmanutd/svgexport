﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SvgApi.DAL;
using SvgApi.Models;
using SvgApi.Services;
using System.Web;
using System.Net.Http.Headers;
using System.IO;

namespace SvgDtoApi.Controllers
{
    [RoutePrefix("api/images")]
    public class SvgDtosController : ApiController
    {
        private SvgContext db;
        public SvgDtosController()
        {
            db = new SvgContext();
        }
        
        [HttpGet]
        [Route("")]
        public IQueryable<SvgDto> GetAll()
        {
            return db.Images;
        }
        
        [ResponseType(typeof(SvgDto))]
        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            SvgDto SvgDto = db.Images.Find(id);
            if (SvgDto == null)
            {
                return NotFound();
            }

            return Ok(SvgDto);
        }
        
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult PutSvgDto(int id, SvgDto SvgDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != SvgDto.Id)
            {
                return BadRequest();
            }

            var xml = PdfCreator.IsValidPdf(SvgDto.SvgString);
            if (xml == null)
            {
                return BadRequest("The SVG definition provided is not a valid XML");
            }

            var isValidSvg = SvgGenerator.XmlIsValidBitmap(xml);
            if (!isValidSvg)
            {
                return BadRequest("The XML provided is not a valid SVG image");
            }

            db.Entry(SvgDto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SvgDtoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(id);
        }
        
        [ResponseType(typeof(SvgDto))]
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostSvgDto(SvgDto SvgDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var xml = PdfCreator.IsValidPdf(SvgDto.SvgString);
            if (xml == null)
            {
                return BadRequest("The SVG definition provided is not a valid XML");
            }

            var isValidSvg = SvgGenerator.XmlIsValidBitmap(xml);
            if (!isValidSvg)
            {
                return BadRequest("The XML provided is not a valid SVG image");
            }

            db.Images.Add(SvgDto);
            db.SaveChanges();

            return Ok(SvgDto.Id);
        }
        
        [HttpDelete]
        [Route("{id}")]
        [ResponseType(typeof(SvgDto))]
        public IHttpActionResult DeleteSvgDto(int id)
        {
            SvgDto SvgDto = db.Images.Find(id);
            if (SvgDto == null)
            {
                return NotFound();
            }

            db.Images.Remove(SvgDto);
            db.SaveChanges();

            return Ok(SvgDto);
        }

        [HttpGet]
        [Route("pdf")]
        public HttpResponseMessage DownloadPdf()
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            var context = new SvgContext();
            var images = context.Images.ToList();

            byte[] buffer;
            try
            {
                buffer = PdfCreator.Generate(images);
            }
            catch (Exception ex)
            {
                response.Content = new StringContent(ex.Message);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }

            //content length for use in header
            var contentLength = buffer.Length;            
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=NiceImages.pdf", out ContentDispositionHeaderValue contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SvgDtoExists(int id)
        {
            return db.Images.Count(e => e.Id == id) > 0;
        }
    }
}