﻿using SvgApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SvgApi.DAL
{
    public class SvgContext : DbContext
    {
        public SvgContext() : base("SvgContext")
        {
        }

        public DbSet<SvgApi.Models.SvgDto> Images { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SvgDto>().ToTable("SvgImages");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}