﻿using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Xml;

namespace SvgApi.Services
{
    public class SvgGenerator
    {
        const float targetWidth = 540;
        public static Bitmap Generate(XmlDocument ducument, string title)
        {
            try
            {
                var svgImage = SvgDocument.Open(ducument);
                var heightScale = (float)svgImage.Height / ((float)svgImage.Width / (float)targetWidth);
                svgImage.Width = targetWidth;
                svgImage.Height = heightScale;
                return svgImage.Draw();
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred converting the SVG {title} to an image.");
            }           
        }
        public static bool XmlIsValidBitmap(XmlDocument ducument)
        {
            SvgDocument document;
            try
            {
                document = SvgDocument.Open(ducument);
            }
            catch(Exception ex)
            {
                return false;
            }

            return document != null;
        }
    }
}