﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Svg;
using SvgApi.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace SvgApi.Services
{
    public class PdfCreator
    {
        public static byte[] Generate(List<SvgDto> data)
        {
            var pdf = new Document(PageSize.A4);

            using (MemoryStream output = new MemoryStream())
            {
                PdfWriter writer =
                    PdfWriter.GetInstance(pdf, output);
                pdf.Open();

                data.ForEach(img =>
                {
                    pdf.NewPage();

                //Parse XML
                var doc = new XmlDocument();

                    try
                    {
                        doc.LoadXml(img.SvgString);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"The XML is not valid for the SVG {img.Title}");
                    }

                    //Generate bitmap from XML
                    var image = SvgGenerator.Generate(doc, img.Title);

                //Create PDF Image
                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image, System.Drawing.Imaging.ImageFormat.Png);

                //Create PDF Paragraph
                iTextSharp.text.Font verdana =
                    FontFactory.GetFont("Verdana", 50, iTextSharp.text.Font.BOLDITALIC, new BaseColor(125, 88, 15));
                    var paragraph = new Paragraph(img.Title, verdana)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    pdf.Add(paragraph);

                    pdf.Add(pdfImage);
                });

                pdf.Close();
                return output.ToArray();
            }
        }

        public static XmlDocument IsValidPdf(string pdf)
        {
            var doc = new XmlDocument();
            try
            {
                doc.LoadXml(pdf);
            }
            catch(Exception ex)
            {
                return null;
            }
            
            return doc;
        }
    }
}