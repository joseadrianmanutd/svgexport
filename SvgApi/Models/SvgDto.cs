﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SvgApi.Models
{
    public class SvgDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string SvgString { get; set; }
    }
}